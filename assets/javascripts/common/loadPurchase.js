const LoadPurchase = {
  init: function() {
    const products = document.querySelectorAll('[data-product-box]:not([data-product-unique-id])')
    const observer = new IntersectionObserver(entries => {
      const intersecting = entries.filter(entry => { if (entry.isIntersecting) return entry })
      if (intersecting.length > 0) {
        const scriptEl = document.createElement('script')
        scriptEl.src = window.purchaseScript
        document.body.appendChild(scriptEl)
        observer.disconnect()
      }
    })

    return products.forEach(product => { observer.observe(product) }, { threshold: 0.1 })
  }
}

export default LoadPurchase
