// ===============================================================
// CONDITIONAL LAZY
// ===============================================================
export default function handleConditionalLazy() {
  const eventType = window.innerWidth <= 1024 ? 'scroll' : 'mousemove';
  window.addEventListener(
    eventType,
    () => {
      const conditionalLazyElements = document.querySelectorAll('[data-conditional-lazy]');
      conditionalLazyElements.length > 0 &&
        conditionalLazyElements.forEach((element) => {
          element.classList.add('lazy');
          element.setAttribute('loading', 'lazy');
          window.lazyLoad && window.lazyLoad.init();
        });
    },
    { once: true }
  );
}
