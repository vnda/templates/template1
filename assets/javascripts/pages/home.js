import setFullbanner from './home/1_fullbanner';
import setIconsVertical from './home/icons_vertical';
import setBannersLinha from './home/home_banner_linha';
import CategoryTabs from './home/3_category_tabs';
import setCarousel from '../components/carousel';
import setBrandsCarousel from './home/brands_carousel';


//addImports

import setTopBar from '../components/topBar';
import handleConditionalLazy from '../components/conditionalLazy';

const Home = {
  init: function () {
    setTopBar();
    handleConditionalLazy();

    setFullbanner()
		setIconsVertical();
		setBannersLinha();
		CategoryTabs.init();
		setCarousel();
		setBrandsCarousel();
		
		
		//calls
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Home.init();
});
