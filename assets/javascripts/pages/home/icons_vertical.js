import Swiper from 'swiper';
import { Pagination, Autoplay } from 'swiper/modules';

export default function setIconsVertical() {
  const sections = document.querySelectorAll('[data-icons-vertical]');

  sections.forEach((section) => {
    const carousel = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');

    const iconsSwiper = new Swiper(carousel, {
      modules: [Pagination, Autoplay],
      slidesPerView: 1,
      spaceBetween: 16,
      watchOverflow: true,
      preloadImages: false,
      speed: 1000,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
        pauseOnMouseEnter: true,
      },
      pagination: {
        el: pagination,
        type: 'bullets',
        clickable: true,
      },
      breakpoints: {
        992: {
          slidesPerView: 3,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 24,
        },
      },
    });

    iconsSwiper.autoplay.stop();

    const initAutoplay = () => {
      iconsSwiper.autoplay.start();

      window.removeEventListener('mousemove', initAutoplay);
      window.removeEventListener('scroll', initAutoplay);
    };

    window.addEventListener('mousemove', initAutoplay);
    window.addEventListener('scroll', initAutoplay);
  });
}
