import Swiper from 'swiper';
import { Navigation, Pagination, Autoplay } from 'swiper/modules';

export default function setFullbanner() {
  const fullbanners = document.querySelectorAll('[data-fullbanner]');

  fullbanners.forEach((section) => {
    const carousel = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');
    const next = section.querySelector('.swiper-button-next');
    const prev = section.querySelector('.swiper-button-prev');

    const newFullbanner = new Swiper(carousel, {
      modules: [Navigation, Pagination, Autoplay],
      slidesPerView: 1,
      watchOverflow: true,
      speed: 1000,
      pagination: {
        el: pagination,
        type: 'bullets',
        clickable: true,
      },
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
        pauseOnMouseEnter: true,
      },
      breakpoints: {
        992: {
          navigation: {
            nextEl: next,
            prevEl: prev,
            disabledClass: '-disabled',
          },
        },
      },
    });

    //Autoplay condicional
    newFullbanner.autoplay.stop();

    const eventType = window.mobile ? 'scroll' : 'mousemove';
    window.addEventListener(eventType, newFullbanner.autoplay.start, { once: true });
  });
}
