import Swiper from 'swiper';
import { Navigation, Pagination } from 'swiper/modules';

const CategoryTabs = {
  handleTabs: function () {
    const tabsContainers = document.querySelectorAll('.category-tabs');

    if (tabsContainers) {
      tabsContainers.forEach((tabsContainer) => {
        const buttons = tabsContainer.querySelectorAll('.button');
        const contentSwipers = tabsContainer.querySelectorAll('.content-swiper');

        if (buttons && contentSwipers) {
          buttons.forEach((button) => {
            button.addEventListener('click', () => {
              buttons.forEach((btn) => btn.classList.remove('-active'));

              const buttonCategory = button.getAttribute('data-button-category');
              button.classList.add('-active');

              contentSwipers.forEach((contentSwiper) => {
                const swiperCategory = contentSwiper.getAttribute('data-swiper-category');
                swiperCategory == buttonCategory
                  ? contentSwiper.classList.add('-active')
                  : contentSwiper.classList.remove('-active');
              });
            });
          });
        }
      });
    }
  },

  handleTabsSwipers: function () {
    const contentSwipers = document.querySelectorAll('[data-swiper-category]');

    contentSwipers.forEach((contentSwiper) => {
      const swiperEl = contentSwiper.querySelector('.swiper');
      const next = contentSwiper.querySelector('.swiper-button-next');
      const prev = contentSwiper.querySelector('.swiper-button-prev');
      const pagination = contentSwiper.querySelector('.swiper-pagination');

      const carousel = new Swiper(swiperEl, {
        modules: [Navigation, Pagination],
        slidesPerView: 2,
        spaceBetween: 8,
        speed: 1000,
        watchOverflow: true,
        pagination: {
          el: pagination,
          type: 'bullets',
          clickable: true,
        },
        breakpoints: {
          768: {
            slidesPerView: 3,
            spaceBetween: 24,
            navigation: {
              nextEl: next,
              prevEl: prev,
            },
          },
          1440: {
            slidesPerView: 4,
            spaceBetween: 24,
            navigation: {
              nextEl: next,
              prevEl: prev,
            },
          },
        },
      });
    });
  },

  init: function () {
    let _this = this;

    _this.handleTabs();
    _this.handleTabsSwipers();
  },
};

export default CategoryTabs;
