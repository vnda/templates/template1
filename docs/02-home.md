- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/02-home.png)

# HOME

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |


&nbsp;

## FULLBANNER PRINCIPAL

**_Informações:_**

| Dúvida                | Instrução                                                        |
| --------------------- | ---------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                          |
| **Onde será exibido** | Banner principal abaixo do header, ocupa 100% da largura da tela |
| **Cadastro exemplo**  | [Admin](https://template1.vnda.dev/admin/midias/editar?id=1) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                            |
| ------------------- | ------------------- | --------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x1012 pixels, Mobile: 1000x1460 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                         |
| **Subtítulo**       | :large_blue_circle: | Título do botão e posição do texto do banner. Opções a baixo!         |
| **Descrição**       | :large_blue_circle: | Título e descrição do banner                                          |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba                |
| **URL**             | :large_blue_circle: | Link de direcionamento                                                |
| **Posição Desktop** | :black_circle:      | `home-banner-principal`                                        |
| **Posição Mobile**  | :black_circle:      | `home-banner-principal-mobile`                                 |
| **Cor**             | :large_blue_circle: |  Cor dos textos                                                       |

&nbsp;

**_Exemplo de subtítulo:_**

```md
CALL TO ACTION | left-center
```

**_Exemplo de descrição:_**

```md
\#\#\# UPPER TITLE
\#\# Título do Banner

Descrição do banner alinhado à esquerda.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

**_Posições do botão possíveis para colocar no campo subtítulo:_**

- left-top
- left-center
- left-bottom

- center-top
- center-center
- center-bottom

- right-top
- right-center
- right-bottom


&nbsp;

## ÍCONES DA HOME

***Informações:***

| Dúvida                          | Instrução                                                    |
| :------------------------------ | :----------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                      |
| **Onde será exibido**           | Seção de carrossel de ícons, abaixo do fullbanner            |
| **Cadastro exemplo em staging** | [Admin](https://template1.vnda.dev/admin/midias/editar?id=4) |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                      |
| ------------- | ------------------- | ----------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 50x50 pixels                 |
| **Título**    | :black_circle:      | Apenas no alt das imagens. Não exibido no front |
| **Subtítulo** | :large_blue_circle: | Título do ícone                                 |
| **Descrição** | :large_blue_circle: | Descrição curta do ícone                        |
| **Externo?**  | :large_blue_circle: |  Selecionar se o link do banner deve abrir em outra aba|
| **URL**       | :large_blue_circle: |  Link de direcionamento                         |
| **Posição**   | :black_circle:      | `home-icones-vertical`                   |
| **Cor**       | :no_entry:          |                                                 |


&nbsp;

## BANNERS DE LINHA (até 3)

***Informações:***

| Dúvida                          | Instrução                                                    |
| :------------------------------ | :----------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                      |
| **Onde será exibido**           | Seção de banners linha, abaixo dos ícones                    |
| **Cadastro exemplo**            | [Admin](https://template1.vnda.dev)                      |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                                |
| ------------- | ------------------- | --------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x1250 pixels                        |
| **Título**    | :black_circle:      | Apenas no alt das imagens. Não exibido no front           |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `home-banner-linha`                                |
| **Cor**      | :no_entry:           |                                                           |

**_Exemplo de descrição:_**

```md
\#\# Categoria

Breve texto descritivo do banner em até duas linhas e centralizado.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## BANNERS ESPELHADOS (até 6)

***Informações:***

| Dúvida                          | Instrução                                                         |
| ------------------------------- | ----------------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                           |
| **Onde será exibido**           | Banners espelhados, abaixo dos banners de linha                   |
| **Cadastro exemplo em staging** | [Admin](https://template1.vnda.dev/)                          |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                                |
| ------------- | ------------------- | --------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x625 pixels                         |
| **Título**    | :black_circle:      | Alt da imagem                                             |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `home-banner-espelhado`                            |
| **Cor**       | :no_entry:          |                                                           |

**_Exemplo de descrição:_**

```md
\#\# TÍTULO DA SEÇÃO

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## TABS DE CATEGORIAS

***Informações:***

| Dúvida                          | Instrução                                              |
| ------------------------------- | ------------------------------------------------------ |
| **Onde cadastrar**              | Menu                                                   |
| **Onde será exibido**           | Tabs de categoria da home                              |
| **Níveis**                      | 1 nível                                                |
| **Cadastro exemplo em staging** | [Admin](https://template1.vnda.com.br/admin/navegacao) |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?     | Orientação                                 |
| ------------- | -------------- | ------------------------------------------ |
| **Título**    | :black_circle: | Título da tab                              |
| **Tooltip**   | :no_entry:     |                                            |
| **Descrição** | :no_entry:     |                                            |
| **Posição**   | :black_circle: | `home-produtos`                            |
| **Externo?**  | :no_entry:     |                                            |
| **URL**       | :black_circle: | Selecionar a tag que irá puxar os produtos |

&nbsp;

## TITULO TABS DE CATEGORIAS

***Informações:***

| Dúvida                          | Instrução                                                       |
| ------------------------------- | --------------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                         |
| **Onde será exibido**           | Título da seção de tabs                                         |
| **Cadastro exemplo em staging** | [Admin](https://template1.vnda.com.br/admin/midias/editar?id=4) |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação           |
| ------------- | ------------------- | -------------------- |
| **Imagem**    | :no_entry:          |                      |
| **Título**    | :black_circle:      | Não exibido no front |
| **Subtítulo** | :large_blue_circle: | Título da seção      |
| **Descrição** | :no_entry:          |                      |
| **Externo?**  | :no_entry:          |                      |
| **URL**       | :no_entry:          |                      |
| **Posição**   | :black_circle:      | `home-titulo-tabs`   |
| **Cor**       | :no_entry:          |                      |


&nbsp;

## BANNER SOBRE

**_Informações:_**

| Dúvida                          | Instrução                                                    |
| ------------------------------- | ------------------------------------------------------------ |
| **Onde cadastrar**              | Banners                                                      |
| **Onde será exibido**           | Banners sobre, abaixo do banner de marcas                    |
| **Cadastro exemplo**            | [Admin](https://template1.vnda.dev/admin/midias/editar?id=47)  |

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                                |
| ------------- | ------------------- | --------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 200x200 pixels                          |
| **Título**    | :black_circle:      | Alt da imagem                                             |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `home-sobre-nos`                                   |
| **Cor**       | :no_entry:          |                                                           |

**_Exemplo de descrição:_**

```md
\#\# Sobre nós

Lorem ipsum dolor sit amet consectetur. Aliquet eu in placerat accumsan nec eget odio. Platea tempus odio sed morbi sit elit. Duis praesent ante est consequat velit. Pellentesque sem eu dictum potenti augue eu interdum. Enim blandit ultricies cras sit tellus. Commodo quisque aliquam tellus in ipsum interdum penatibus.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## BANNER VÍDEO

**_Informações:_**

| Dúvida                          | Instrução                                                    |
| ------------------------------- | ------------------------------------------------------------ |
| **Onde cadastrar**              | Banners                                                      |
| **Onde será exibido**           | Banner de vídeo, abaixo do banner sobre                      |
| **Cadastro exemplo**            | [Admin](https://template1.vnda.dev/admin/midias/editar?id=46) |

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                          |
| ------------------- | ------------------- | ------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2220x925 pixels. Mobile: 1000x845 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                       |
| **Subtítulo**       | :large_blue_circle: | Título da seção                                                     |
| **Descrição**       | :no_entry:          |                                                                     |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba              |
| **URL**             | :large_blue_circle: | Link de direcionamento ou link do vídeo                                             |
| **Posição Desktop** | :black_circle:      | `home-banner-video`                                          |
| **Posição**         | :black_circle:      | `home-banner-video-mobile`                                   |
| **Cor**             | :no_entry:          |                                                                     |

&nbsp;

**OBSERVAÇÃO**: O link de direcionamento só funciona para imagem. Se um vídeo for cadastrado na url, a imagem não será exibida
&nbsp;

## INSTAGRAM - TEXTO CENTRALIZADO

**_Informações:_**

| Dúvida                          | Instrução                                           |
| ------------------------------- | --------------------------------------------------- |
| **Onde cadastrar**              | Banners                                             |
| **Onde será exibido**           | Texto da seção do instagem                          |
| **Cadastro exemplo em staging** | [Admin](https://template1.vnda.dev)             |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?     | Orientação             |
| ------------- | -------------- | ---------------------- |
| **Imagem**    | :large_blue_circle: | Ícone exibido ao lado do título. Dimensões sugeridas de 64x64 pixels |
| **Título**    | :black_circle:      | Utilizado para controle interno |
| **Subtítulo** | :large_blue_circle: | @ da marca \| texto do botão. Ex.: "@marcainstagram \| Seguir +" |
| **Descrição** | :large_blue_circle: | Breve texto exibido abaixo do título  |
| **Externo?**  | :no_entry:          |                        |
| **URL**       | :black_circle:      | Link para redirecionamento |
| **Posição**   | :black_circle:      | `home-instagram-texto` |
| **Cor**       | :no_entry:          |                        |

&nbsp;

**_Observações:_**

As imagens utilizadas no ambiente de desenvolvimento e pré produção, são apenas para demarcar a seção.
Para está seção aparecer em produção é necessário realizar a integração com o instagram. Caso deseje está opção, entrar em contato com o atendimento.

***


***
